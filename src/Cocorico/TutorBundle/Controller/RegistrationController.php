<?php

namespace Cocorico\TutorBundle\Controller;

use Cocorico\TutorBundle\Form\Type\RegistrationFormType;
use Cocorico\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Cocorico\UserBundle\Form\Handler\RegistrationFormHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("tutor")
 */
class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="tutor_register")
     * @Template()
     */
    public function indexAction()
    {
        $session = $this->container->get('session');
        $router = $this->container->get('router');
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            if (!$session->has('profile')) {
                $session->set('profile', 'asker');
            }
            $url = $router->generate('cocorico_home');

            return new RedirectResponse($url);
        } else {
            $user = new User();
            $form = $this->createForm(RegistrationFormType::class, $user);
            /** @var RegistrationFormHandler $formHandler */
            $formHandler = $this->container->get('fos_user.registration.form.handler');
            $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

            $process = $formHandler->process($confirmationEnabled);
            if ($process) {
                $user = $form->getData();

                $session->getFlashBag()->add(
                    'success',
                    $this->container->get('translator')->trans('user.register.success', array(), 'cocorico_user')
                );

                if ($confirmationEnabled) {
                    $session->set('cocorico_user_send_confirmation_email/email', $user->getEmail());
                    $url = $router->generate('cocorico_user_registration_check_email');
                } else {
                    $url = $router->generate('cocorico_user_register_confirmed');
                }

                return new RedirectResponse($url);
            }

            return array('form' => $form->createView());
        }

    }
    
    /**
     * @Route("/verify-phone-number", name="verify_phone_number", options={"expose"=true})
     * @param Request $request
     * @return JsonResponse
     */
    public function verifyAction(Request $request)
    {
        //TODO Implement test verification
        $status = $request->isXmlHttpRequest() && $request->request->get('code') === '1111' ? true : false;
        $response = array('status' => $status);
        
        return new JsonResponse($response);
    }
}
