<?php

namespace Cocorico\TutorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TutorController extends Controller
{
    /**
     * @Route("/tutors", name="cocorico_tutors_index")
     * @Template()
     */
    public function indexAction()
    {
        $name = "asd";
        return array('name' => $name);
    }
}
