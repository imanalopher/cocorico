<?php

namespace Cocorico\TutorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileContactFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                'email',
                array(
                    'label' => 'form.user.email'
                )
            )
            ->add(
                'phone',
                'text',
                array(
                    'label' => 'form.user.phone',
                    'required' => false
                )
            )
            ->add(
                'addresses',
                'collection',
                array(
                    'type' => new TutorAddressFormType(),
                    /** @Ignore */
                    'label' => false,
                    'required' => false,
                )
            );


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Cocorico\UserBundle\Entity\User',
                'csrf_token_id' => 'profile',
                'translation_domain' => 'cocorico_user',
                'cascade_validation' => true,
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tutor_profile_contact';
    }
}
