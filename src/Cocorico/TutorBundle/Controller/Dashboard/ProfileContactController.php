<?php

namespace Cocorico\TutorBundle\Controller\Dashboard;

use Cocorico\TutorBundle\Form\Type\ProfileContactFormType;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ProfileController
 *
 * @Route("/tutor")
 */
class ProfileContactController extends Controller
{
    /**
     * Edit user profile
     *
     * @Route("/edit-contact", name="cocorico_tutor_dashboard_profile_edit_contact")
     * @Method({"GET", "POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $formHandler = $this->get('cocorico_user.form.handler.contact');
        $user = $formHandler->init($user);

        $form = $this->createContactForm($user);
        $success = $formHandler->process($form);

        $session = $this->container->get('session');
        $translator = $this->container->get('translator');

        if ($success > 0) {
            $session->getFlashBag()->add(
                'success',
                $translator->trans('user.edit.contact.success', array(), 'cocorico_user')
            );

            return $this->redirect(
                $this->generateUrl(
                    'cocorico_tutor_dashboard_profile_edit_contact'
                )
            );
        } elseif ($success < 0) {
            $session->getFlashBag()->add(
                'error',
                $translator->trans('user.edit.contact.error', array(), 'cocorico_user')
            );
        }

        return $this->render(
            'CocoricoTutorBundle:Dashboard/Profile:edit_contact.html.twig',
            array(
                'form' => $form->createView(),
                'user' => $user,
            )
        );
    }

    /**
     * Creates a form to edit a user entity.
     *
     * @param mixed $user
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createContactForm($user)
    {
        $form = $this->get('form.factory')->createNamed(
            'user',
            new ProfileContactFormType(),
            $user,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl('cocorico_tutor_dashboard_profile_edit_contact'),
            )
        );

        return $form;
    }
}
