<?php

/*
 * This file is part of the Cocorico package.
 *
 * (c) Cocolabs SAS <contact@cocolabs.io>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cocorico\TutorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileBankAccountFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'lastName',
                'text',
                array(
                    'label' => 'form.user.last_name',
                )
            )
            ->add(
                'firstName',
                'text',
                array(
                    'label' => 'form.user.first_name'
                )
            )
            ->add(
                'birthday',
                'birthday',
                array(
                    'label' => 'form.user.birthday',
//                    'format' => 'dd MMMM yyyy',
                    'widget' => "choice",
                    'years' => range(date('Y') - 18, date('Y') - 100),
                    'required' => true
                )
            )
            ->add(
                'nationality',
                'country',
                array(
                    'label' => 'form.user.nationality',
                    'required' => false,
                    'preferred_choices' => array("GB", "FR", "ES", "DE", "IT", "CH", "US", "RU"),
                )
            )
            ->add(
                'gender',
                'choice',
                array(
                    'label' => 'Gender',
                    'required' => false,
                    'empty_value' => 'Choose a gender',
                    'choices' => array(
                        'm' => 'Male',
                        'f' => 'Female',
                    ),
                )
            )
            ->add(
                'experience',
                'number',
                array(
                    'required' => false,
                    'label' => 'Experience (years)',
                )
            )
            ->add(
                'profession',
                'text',
                array(
                    'label' => 'form.user.profession',
                    'required' => false
                )
            )
            ->add(
                'university',
                'text',
                array(
                    'label' => 'College/University',
                    'required' => false
                )
            )
            ->add(
                'degree',
                'text',
                array(
                    'label' => 'Name of Degree',
                    'required' => false
                )
            )
            ->add(
                'subject_taken',
                'text',
                array(
                    'label' => 'Subject taken',
                    'required' => false
                )
            )
            ->add(
                'passing_year',
                'text',
                array(
                    'label' => 'Passing Year',
                    'required' => false
                )
            )
            ->add(
                'certificate',
                'file',
                array(
                    'label' => 'Upload Degree Certificate',
                    'required' => false
                )
            )
        ;


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Cocorico\UserBundle\Entity\User',
                'csrf_token_id' => 'CocoricoProfileBankAccount',
                'translation_domain' => 'cocorico_user',
                'cascade_validation' => true,
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tutor_profile_bank_account';
    }
}
