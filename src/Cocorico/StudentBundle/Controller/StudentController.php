<?php

namespace Cocorico\StudentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class StudentController extends Controller
{
    /**
     * @Route("/students", name="cocorico_student_index")
     * @Template()
     */
    public function indexAction()
    {
//        $students = $this->getDoctrine()->getRepository('CocoricoUserBundle:User');
        $pageName = "Student's Page";
        return array('pageName' => $pageName);
    }
}
